# Description

This repos is a test repo for the library ``odoo-openupgrade-wizard``.
(https://gitlab.com/odoo-openupgrade-wizard/odoo-openupgrade-wizard_framework)

The objective is to download the entire openupgrade framework for all the versions,
and to generate upgrade analysis files.

# Basics commands


```
oow init \
  --initial-version 8.0
  --final-version 16.0
  --project-name test-framework
  --extra-repository OCA/web,OCA/server-tools

```
